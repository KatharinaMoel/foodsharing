Das Jahr 2024 beginnt für die Foodsharing-Webseite direkt mit einem großen Update: **Release Kiwi**!
Wir haben (wie immer) versucht, an allen Ecken und Enden der Webseite Bestehendes zu verbessern und neue Funktionen umzusetzen. Hoffentlich können wir uns allen damit die Arbeit erleichtern und dazu beitragen, dass wir uns noch effektiver gegen die Lebensmittelverschwendung einsetzen!

Die folgende Liste soll einen Überblick darüber geben, was sich in den letzten 4 Monaten getan hat, ist aber keinesfalls vollständig. All die Fehlerbehebungen, Änderungen an Texten und nach außen hin unsichtbaren Verbesserungen am Code sind zwar sehr wichtig, aber für die meisten eher nicht so spannend. Wer dennoch Interesse daran hat, kann einen Blick in das vollständige [Changelog](/content?sub=changelog) werfen, wo alle erledigten Aufgaben und verantwortlichen Personen verlinkt sind. Jeder Eintrag enthält auch einen Link zu der relevanten Änderung am Open Source Code der foodsharing Webseite.

### Forum
- Der Titel von Themen im Forum kann jetzt bearbeitet werden. Die Berechtigung dazu haben der/die Ersteller:in des Themas und Moderator:innen des Forums. ( !2882)
- Ein Button, mit dem an den Anfang oder das Ende eines Threads gesprungen werden kann, ist nun von überall im Thread aus sichtbar, sobald man anfängt, in die entsprechende Richtung zu scrollen. Damit ist es leichter, in langen Threads zu navigieren. ( !2882)
- Die Möglichkeit, Themen zu abonnieren und damit auf dem Laufenden zu bleiben, ist nun deutlich sichtbarer. ( !2882)
- Angepinnte Themen werden mit einem Icon markiert ( !2882)
- In der Themenübersicht werden angepinnte Beiträge nach Namen sortiert. Das erlaubt es Moderator:innen des Forums, diese wie gewünscht zu sortieren. ( !2883, !2891)
- Die Glocken, die bei neuen Forenbeiträgen versendet werden, wurden verbessert: ( !2827, !2826)
    - Mehrere neue Beiträge werden jetzt zu einer Glocke zusammengefasst.
    - Es werden keine Glocken zu gelöschten Forenbeiträgen mehr angezeigt.
    - Ein größerer Teil des Forums-Titels wird angezeigt 
- Moderator:innen können jetzt auch in geschlossenen Foren schreiben, statt diese vorher öffnen und hinterher wieder schließen zu müssen. ( !3019)
- Beiträge können nun auch in geschlossenen Foren gelöscht werden. Berechtigt sind dazu nach wie vor die Autor:innen und [Orga-Nutzer](https://wiki.foodsharing.de/Benutzerrolle_:_Orga). ( !3019)
- Auch in geschlossenen Foren kann jetzt auf Nachrichten reagiert werden. ( !3019)
- Nutzt man den Antworten-Button unter Beiträgen, werden diese in der eigenen Nachricht zitiert. ( !2937)
- Der Quelltext formatierter Forenbeiträge kann kopiert werden. ( !2913)
- Das Versenden von E-Mails zu neuen Forenbeiträgen muss nun explizit bestätigt werden, um unnötig versendete E-Mails zu vermeiden. ( !2877)
- Texte zu formatieren ist jetzt viel einfacher. Mehr Infos dazu im Abschnitt *Textformatierung*.

### Textformatierung
Schon seit einiger Zeit ist es an verschiedenen Stellen möglich, eingegebene Texte durch Sonderzeichen zu formatieren. Diese Formatierung heißt [Markdown](https://markdown.de) und ist weit verbreitet. Jetzt gibt es eine neue Texteingabe, mit der Texte auch ohne Erfahrung mit Markdown leicht formatiert werden können. Überschriften, Textformatierungen, Links, Trennlinien, Zitate, Listen und mehr können damit leicht eingefügt werden.
Da es trotzdem nicht immer leicht ist, sich vorzustellen, wie der fertig formatierte Text aussehen wird, kann der geschriebene Text vor dem Senden formatiert angesehen werden. Der Vorschau-Modus wird mit dem kleinen Auge ganz rechts in der Toolbar des Textfelds ein- und ausgeschaltet.

Überall, wo Texte mit Markdown formatiert werden können, wird diese neue Texteingabe genutzt. Das bedeutet:
Betriebsbeschreibung, Betriebspinnwand, Bezirksbeschreibung, Erstellen und Bearbeiten von Abstimmungen und natürlich das Forum sind jetzt einfacher zu benutzen. ( !2865)

### Verwaltung von Betrieben
- Die Betriebsinformationen und -einstellungen sind jetzt in einem eigenen Tab der Betriebsseite aufgeführt und nicht mehr in einem Popup. Damit ist mehr Platz und die Bearbeitung der Daten ist übersichtlicher. ( !2874) 
- Das Dropdown, um Slots zu bestätigen oder Foodsaver:innen auszutragen, wurde in ein Popup geändert. Dort werden zusätzliche relevante Nutzerdaten angezeigt, wie Anzahl der Abholungen sowie der aktuell belegten Slots im Betrieb, Datum der letzten Abholung und der Eintragungszeitpunkt. Betriebsverantwortliche können so leichter entscheiden, welche Slots sie bestätigen, und damit betriebsinterne Regeln umsetzen. ( !2813)
- Betriebsverantwortliche bekommen angezeigt, wie weit Bewerber:innen vom Betrieb entfernt wohnen, sodass bevorzugt Foodsaver:innen mit kurzem Weg Abholungen übernehmen können. ( !2772)
- Betriebsverantwortliche haben nun Einsicht in ein Protokoll aller relevanten Aktionen in ihrem Betrieb. Damit werden Aktionen nachvollziehbar und Konflikte können besser gelöst werden. ( !2902)
- Unbestätigt gebliebene Eintragungen in Slots des vergangenen Tages werden nun nicht mehr automatisch gelöscht, sondern bleiben erhalten. ( !2901)
- Betriebsverantwortliche können Posts auf der Pinnwand ihrer Betriebe nun direkt löschen, und nicht mehr erst nach einem Monat. Damit können nicht mehr relevante Posts früher gelöscht werden und Betriebsverantwortliche die Pinnwand aufgeräumt halten. ( !2956)
- Botschafter:innen oder Mitglieder der Betriebs-Koordinations-AG können nun mehr als 3 Betriebsverantwortliche eintragen oder den/die letzte/n BV austragen. Damit ist es in Ausnahmefällen möglich, mehr als 3 Personen mit BV-Rechten auszustatten, und kein:e Betriebsverantwortliche:r muss mehr im Team inaktiver Betriebe verbleiben. ( !2895)

### Suchfunktion
Die Funktionsweise der Suchfunktion wurde verändert, sodass möglichst relevante Ergebnisse angezeigt werden. Zusätzlich hat die Suche viele neue Funktionen erhalten: 
- Zwei neue Ergebnistypen: Jetzt können auch Chats und Forumsthemen über die Suche gefunden werden.
- Alle Ergebnistypen enthalten mehr Informationen und Möglichkeiten, direkt zu den gesuchten Inhalten zu navigieren:
    - **Betriebe**: Kooperationsstatus, Adresse, Bezirk und dein Mitgliedsstatus werden angezeigt.
    - **Personen**: ID, Stammbezirk und ob die Person noch nicht verifiziert ist, werden angezeigt. Außerdem kannst du direkt einen Chat mit der gesuchten Person öffnen und diese aus dem Suchfenster heraus anrufen, falls ihr in einem gemeinsamen Betriebsteam seid.
    - **Bezirke und Arbeitsgruppen**: Botschafter:innen bzw. Admins, E-Mail-Adresse und übergeordnete/r Bezirk / AG werden angezeigt. Damit ist es nicht mehr nötig, Bezirken beizutreten, nur um die Botschafter:innen zu kontaktieren.
    - **Themen**: Titel, Zeitpunkt des letzten Beitrags und das zugehörige Forum werden angezeigt. Themen können nur anhand ihres Titels gefunden werden.
    - **Chats**: Mitglieder des Chats und die letzte Nachricht werden angezeigt. Chats können auch anhand der Vornamen der Mitglieder gesucht werden.
- Wird mehr als ein Suchwort eingegeben, werden weitere Suchkriterien genutzt:
    - Die Suche nach Betrieben, Personen, Bezirken, AGs und Themen kann durch den Namen des (übergeordneten / Stamm-)Bezirks eingeschränkt werden. Mit den Suchbegriffen **`Jan Münster`** könnten bspw. Jan oder Jana mit Stammbezirk Münster gefunden werden.
    - Die Suche nach Betrieben und Fairteilern kann durch deren Adresse eingeschränkt werden. Mit der Suche nach **`Superbiomarkt Badstr`** könnte der SuperBioMarkt mit Adresse an der Badstraße gefunden werden.
- Die Ergebnisse sind nutzer:innenspezifisch. So werden bspw. nicht mehr alle Nutzer:innen angezeigt, die auf den Suchbegriff passen, sondern nur die, mit denen man einen "Berührungspunkt" hat - Eine gemeinsame Stadt, AG oder Betrieb, wo ihr aktiv seid, oder Menschen, die ihr als Bekannte markiert habt. Personen mit häufigen Namen oder Kettenbetriebe sind so leichter zu finden.
- Wie gehabt muss jedes Suchwort irgendwo in den Suchkriterien vorkommen, damit ein Ergebnis gefunden wird. Damit könnte bspw. auch Johannes aus Neumünster gefunden werden, wenn man nach **`Hannes Münster`** sucht. Gerade wenn man nicht genau weiß, wie jemand oder etwas heißt, ist das sehr praktisch. Falls die Suche Wortanfänge oder -enden berücksichtigen soll, kann das mit Anführungszeichen erreicht werden: Während man mit der Suche nach **`Anne`** neben Anne auch Anneke oder Marianne finden könnte, findet man mit **`"Anne`** nur noch Anne und Annette, mit der Suche nach **`"Anne"`** tatsächlich nur noch Anne.
- Schnellere Navigation: Die Suche kann mit **`Shift+F`** direkt geöffnet und Suchergebnisse mit **`Tab`** ausgewählt werden.
- Übersichtliche Ergebnisliste: Pro Ergebnistyp werden zunächst nur wenige Ergebnisse angezeigt, sodass die Liste nicht zu lang wird. Weitere Ergebnisse können ausgeklappt werden.
- Es ist wieder möglich, Personen über ihren Nachnamen zu suchen. Aus Datenschutzgründen müssen dazu aber mindestens die ersten 4 Buchstaben des Nachnamens eingegeben werden, oder der gesamte Nachname, falls dieser kürzer als 4 Buchstaben ist.
( !2886, !2914, !2963, !3023)

### Weitere neue Funktionen
- Im Kalendermodul kann ausgewählt werden, ob der Kalender auch Einladungen zu Terminen enthalten soll, die noch nicht beantwortet wurden. ( !2873)
- Bei der Erstellung von Essenskörben kann jetzt eine Adresse eingegeben werden, die statt der privaten Adresse genutzt werden soll. Damit ist es möglich, Essenskörbe anzubieten, ohne die private Adresse öffentlich sichtbar zu machen. ( !2906)
- foodsharing gibt es jetzt auch auf Spanisch. ( !2892)
- Nutzer mit ungültiger Telefonnummer werden auf dem Dashboard darauf hingewiesen. ( !2916)
- Das Hinzufügen von Bekannten muss jetzt bestätigt werden. Außerdem können Bekanntschaften wieder aufgelöst werden. ( !2919)
- Orga-Rechte von Nutzer:innen werden nun auf deren Profilseite angezeigt. Siehe als Beispiel das Profil von [Frank](/profile/6766). ( !2955)
- Die angezeigten Spalten (sowie deren Reihenfolge) in verschiedenen Übersichtstabellen sind jetzt konfigurierbar. Änderungen an der Anzeige können gespeichert werden, und die Tabellen wurden für schmale Displays optimiert. Diese Neuerung bezieht sich auf die Liste eigener Betriebe, regionaler Betriebe und Betriebsketten. ( !2733)

### Weitere Verbesserungen
- In der Team-Liste in Betrieben ist der Button zum „Anrufen“ jetzt auch auf Geräten mit breitem Bildschirm sichtbar. ( !2835)
- Die Buttons für die Chats und zum Verlassen des Teams wurden angepasst. ( !2874)
- Das Modul "E-Mail-Postfächer" wurde vollständig mit aktuellen Technologien überarbeitet. ( !2580)
- Die Anzeige und Bedienung bei Bewertungswahlen wurde verbessert. ( !2920)
- Zu Terminen werden nun automatisch auch Nutzer:innen eingeladen, die nach Erstellung des Termins der Region beitreten. ( !2926)
- Die Seite, auf der bearbeitbare Inhalte aufgelistet werden, wurde überarbeitet. ( !2862)
- Die E-Mail, die bei einer neuen Bewerbung für eine Arbeitsgruppe an die Admins dieser Gruppe gesendet wird, enthält jetzt einen Link, der direkt zur Bewerbung führt. ( !2973)
- Es wurde der Typ "Wahlen" zu den Arbeitsgruppenfunktionen zu GOALS hinzugefügt. Die Admins dieser AG erscheinen in der Bezirksübersicht. ( !3001)
- Die Admins einer lokal angelegten GOALS-AG Wahlen werden mit dem Nachtlauf automatisch der überregionalen Wahlen-Praxisaustausch AG hinzugefügt. ( !3031)
- Das Standardavatar und das Gruppensymbol wurden durch etwas inklusivere Varianten ersetzt. ( !3009, !3032)
